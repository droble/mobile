//
//  MainViewController.swift
//  mobile-test
//
//  Created by Daniel Roble on 02-06-17.
//  Copyright © 2017 Daniel Roble. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource , UISearchResultsUpdating{
    
    var tagsList = Array<String>()
    var filteredTagsList = Array<String>()
    @IBOutlet weak var tableview: UITableView!
    var resultController = UITableViewController()
    var searchController = UISearchController()
    
    // MARK: - LifeCyle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create a new tableViewController for display result
        self.searchController.loadViewIfNeeded() // 
        self.resultController.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        self.resultController.tableView.delegate  = self;
        self.resultController.tableView.dataSource  = self;
        
        // Setting SearchController
        self.searchController = UISearchController(searchResultsController: resultController)
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
        
        // Adding SearchController on tableViewHeader
        self.tableview.tableHeaderView = searchController.searchBar
        
        // Need this for remove blank space on table
        definesPresentationContext = true
        
        // Request Tags
        ImgurService.shareInstance.getTags { (tags,error) in
            if error != nil{
                let alert = UIAlertController(title: "Ops", message: "Something went wrong", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            self.tagsList = tags as! Array<String>
            self.tableview?.reloadData()
            
        }        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive {
            return filteredTagsList.count;
        }
        return tagsList.count;
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        if searchController.isActive && searchController.searchBar.text != ""{
            cell.textLabel?.text = filteredTagsList[indexPath.row]
        } else {
            cell.textLabel?.text = tagsList[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "galleryByTags", sender: self)
    }
    
    // MARK: - Search Bar
    func updateSearchResults(for searchController: UISearchController){
        filterTagForSearch(searchText: searchController.searchBar.text!)
    }
    
    func filterTagForSearch(searchText: String) {
        filteredTagsList = tagsList.filter({ (text) in
            return text.contains(searchText.lowercased())
        })
        self.resultController.tableView.reloadData()
    }
    
    
    // MARK: - Navigation    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "galleryByTags" {
            let nextView = segue.destination as? GalleryByTagsViewController
            var tagsSelected : String?
            if searchController.isActive && searchController.searchBar.text != "" {
                if let indexPath = self.resultController.tableView.indexPathForSelectedRow {
                    tagsSelected = filteredTagsList[indexPath.row]
                }
            }else{
                if let indexPath = self.tableview.indexPathForSelectedRow {
                    tagsSelected = tagsList[indexPath.row]
                }
            }
            nextView?.tagsSelected = tagsSelected
        }
    }
 

}
