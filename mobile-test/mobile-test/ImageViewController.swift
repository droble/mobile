//
//  ImageViewController.swift
//  mobile-test
//
//  Created by Daniel Roble on 06-06-17.
//  Copyright © 2017 Daniel Roble. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    var imageGallery : ImgurGallery!
    var commentsList = Array<ImgurComment>()
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var upsLabel: UILabel!
    @IBOutlet weak var downLabel: UILabel!
    @IBOutlet weak var viewsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        self.downLabel.text = imageGallery.downs
        self.upsLabel.text = imageGallery.ups
        self.viewsLabel.text = imageGallery.views
        self.tableview.rowHeight = UITableViewAutomaticDimension
        self.tableview.estimatedRowHeight = 300
        
        ImgurService.shareInstance.getImagefrom(url: imageGallery.linkFirstImageFromAlbum!) { (image, error) in
            if error != nil{
                let alert = UIAlertController(title: "Ops", message: "Something went wrong", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            self.imageView.image = image
        }
        
        ImgurService.shareInstance.getCommentFromAlbumId(idAlbum: imageGallery.id) { (arrayComments, error) in
            self.commentsList = arrayComments! as Array<ImgurComment>
            self.tableview.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return commentsList.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CommentTableViewCell
        if indexPath.row == 0 {
            // Description
            cell.Title.text = "Description"
            cell.subTitle.text = imageGallery.description
        }else{
            // Comment
             let comment : ImgurComment = commentsList[indexPath.row]
            cell.Title.text = comment.author
            cell.subTitle.text = comment.comment //ops 
        }
        return cell
    }
}
