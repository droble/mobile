//
//  GalleryTableViewCell.swift
//  mobile-test
//
//  Created by Daniel Roble on 04-06-17.
//  Copyright © 2017 Daniel Roble. All rights reserved.
//

import UIKit

class GalleryTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewGallery: UIImageView?
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var descriptionLabel: UILabel?
    @IBOutlet weak var dateTimeLabel: UILabel?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.descriptionLabel?.textAlignment = NSTextAlignment.center;
        self.descriptionLabel?.numberOfLines = 0;
        // Configure the view for the selected state
    }

}
