//
//  GalleryByTagsViewController.swift
//  mobile-test
//
//  Created by Daniel Roble on 04-06-17.
//  Copyright © 2017 Daniel Roble. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class GalleryByTagsViewController: UIViewController , UITableViewDelegate, UITableViewDataSource , UISearchResultsUpdating  ,UISearchBarDelegate{
    
    var tagsSelected : String?
    var galleryList = Array<ImgurGallery>()
    var filteredGalleryList = Array<ImgurGallery>()
    @IBOutlet weak var tableview: UITableView!
    var searchController = UISearchController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = tagsSelected
        // Setting SearchController
        self.searchController.loadViewIfNeeded()
        self.searchController = UISearchController(searchResultsController: nil) // using the same tableView
        self.searchController.searchBar.delegate = self
        self.searchController.searchBar.scopeButtonTitles = ["All", "Title", "Description"]
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.hidesNavigationBarDuringPresentation = false
        // Adding SearchController on tableViewHeader
        self.tableview.tableHeaderView = searchController.searchBar
        self.definesPresentationContext = true
        // adding simple indicator
        let activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 0 , y: 0, width: 100, height: 100), type: .ballScaleMultiple, color: UIColor.blue, padding: nil)
        activityIndicatorView.center = self.view.center
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        
        ImgurService.shareInstance.getGallery(tagSelected: tagsSelected!) { (gallery, error) in
            if error != nil{
                let alert = UIAlertController(title: "Ops", message: "Something went wrong", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            self.galleryList = gallery as! Array<ImgurGallery>
            activityIndicatorView.stopAnimating()
            self.tableview.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive  && searchController.searchBar.text != ""{
            return filteredGalleryList.count;
        }
        return self.galleryList.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableview.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! GalleryTableViewCell
        var gallery : ImgurGallery!
        if searchController.isActive && searchController.searchBar.text != ""{
            gallery  = filteredGalleryList[indexPath.row]
        }else{
            gallery  = galleryList[indexPath.row]
        }
        cell.titleLabel?.text = gallery.title
        cell.descriptionLabel?.text = gallery?.description
        cell.dateTimeLabel?.text = gallery!.dateTime
        let pathURL = URL(string: (gallery?.linkFirstImageFromAlbum)!)!
        cell.imageViewGallery?.af_setImage(withURL:pathURL)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "imageView", sender: self)
    }
     // MARK: - UISearchBar
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterTagForSearch(searchText: searchController.searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
    
    func updateSearchResults(for searchController: UISearchController){
        let searchBar = searchController.searchBar
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterTagForSearch(searchText: searchController.searchBar.text!, scope: scope)
    }
    
    func filterTagForSearch(searchText: String, scope: String) {
        filteredGalleryList = galleryList.filter({( gallery : ImgurGallery) -> Bool in
            
            switch scope{
            case "Title":
                return gallery.title.lowercased().contains(searchText.lowercased())
            case "Description":
                return gallery.description!.lowercased().contains(searchText.lowercased())
            default:
                return gallery.description!.lowercased().contains(searchText.lowercased()) || gallery.title.lowercased().contains(searchText.lowercased())
            }        
        })
        self.tableview.reloadData()
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "imageView" {
            let nextView = segue.destination as? ImageViewController
            var gallery : ImgurGallery!
            if searchController.isActive && searchController.searchBar.text != "" {
                if let indexPath = self.tableview.indexPathForSelectedRow {
                    gallery = filteredGalleryList[indexPath.row]
                }
            }else{
                if let indexPath = self.tableview.indexPathForSelectedRow {
                    gallery = galleryList[indexPath.row]
                }
            }
            nextView?.imageGallery = gallery
        }
    }

}
