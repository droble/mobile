//
//  ImgurComment.swift
//  mobile-test
//
//  Created by Daniel Roble on 06-06-17.
//  Copyright © 2017 Daniel Roble. All rights reserved.
//

import Foundation
class ImgurComment {
    // imgur GalleryModel
    
    var author : String!
    var comment : String!
    
    init(author:String, comment:String ) {
        self.author = author
        self.comment = comment
    }
    
}
