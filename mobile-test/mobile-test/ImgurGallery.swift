//
//  ImgurGallery.swift
//  mobile-test
//
//  Created by Daniel Roble on 04-06-17.
//  Copyright © 2017 Daniel Roble. All rights reserved.
//

import Foundation

class ImgurGallery {
    // imgur GalleryModel
    var id : String!
    var title : String!
    var dateTime : String!
    // imgur Album Model
    var description : String?
    var linkFirstImageFromAlbum: String?
    var ups : String?
    var downs : String?
    var views : String?
    // end point https://api.imgur.com/3/gallery/yVuZJ/comments/
    var comment : String?
    var author : String?
    
    init(id:String, title:String , dateTime:Int ) {
        self.id = id
        self.title = title
        // Transform datetime
        let date = Date(timeIntervalSince1970: TimeInterval(dateTime))
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .medium
        self.dateTime = dateFormatter.string(from: date)        
    }
    
}
