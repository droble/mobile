//
//  ImgurService.swift
//  mobile-test
//
//  Created by Daniel Roble on 01-06-17.
//  Copyright © 2017 Daniel Roble. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireImage

class ImgurService {
    
    enum ImgurError: Error {
        case failure
    }
    
    static let shareInstance  = ImgurService()
    let sessionManager : SessionManager
    let clientID : String?
    
    private init() {
        var defaultHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        defaultHeaders["authorization"] = "Client-ID 59bfee277dd9ef3"
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = defaultHeaders
        sessionManager = Alamofire.SessionManager(configuration: configuration)
        clientID = "59bfee277dd9ef3"
    }
  
    func getTags(completion:@escaping (Array<Any>,Error?) -> ()) {
        sessionManager.request("https://api.imgur.com/3/tags").responseJSON { response in
            if((response.result.value) != nil) {
                let json = JSON(response.result.value!)
                let arrayTags =  json["data"]["tags"].arrayValue.map({$0["name"].stringValue})
                completion(arrayTags,nil)
            }else{
                completion([],ImgurError.failure)
            }
        }
    }
    
    func getGallery(tagSelected:String, completion:@escaping (Array<Any>,Error?) -> ()) {
        let url = "https://api.imgur.com/3/gallery/t/" + tagSelected
        sessionManager.request(url).responseJSON { response in
            if((response.result.value) != nil) {
                let json = JSON(response.result.value!)
                let dispacthGallery = DispatchGroup()
                var arrayGallery = Array<ImgurGallery>()
                for (_,subJson):(String, JSON) in json["data"]["items"] {
                    dispacthGallery.enter()
                    let gallery = ImgurGallery(id: subJson["id"].stringValue, title: subJson["title"].stringValue, dateTime: subJson["datetime"].intValue)
                    self.getAlbumFromId(idAlbum: gallery.id!, completion: { (url,description,ups,downs,views, error) in
                        // if Empty, just ignore
                        if error == nil{
                            gallery.linkFirstImageFromAlbum = url
                            // I prefer display image description
                            gallery.description = description
                            gallery.ups = ups
                            gallery.downs = downs
                            gallery.views = views
                            arrayGallery.append(gallery)
                            dispacthGallery.leave()
                        }else{
                            dispacthGallery.leave()
                        }                        
                    })
                }
                dispacthGallery.notify(queue: .main){
                    completion(arrayGallery,nil)
                }
            }else{
                completion([],ImgurError.failure)
            }
        }
    }
    
    func getAlbumFromId(idAlbum : String,completion:@escaping(String?,String?,String?,String?,String?,Error?) -> ()){
        let url = "https://api.imgur.com/3/gallery/album/" + idAlbum
        sessionManager.request(url).responseJSON { response in
            if((response.result.value) != nil) {
                let json = JSON(response.result.value!)
                //  If description is empty not generate a crash, so isn't check
                let description =  json["data"]["images"][0]["description"].stringValue
                let url =  json["data"]["images"][0]["link"].stringValue
                let ups =  json["data"]["ups"].stringValue
                let downs =  json["data"]["downs"].stringValue
                let views =  json["data"]["views"].stringValue
                // For some reasons, there are album that don't have images
                if (url.isEmpty != true) {
                    completion(url,description,ups,downs,views,nil)
                }else{
                    completion(nil,nil,nil,nil,nil,ImgurError.failure)
                }
            }else{
                completion(nil,nil,nil,nil,nil,ImgurError.failure)
            }
        }
    }
    
    func getCommentFromAlbumId(idAlbum : String , completion:@escaping(Array<ImgurComment>?,Error?) -> ()){
        let url = "https://api.imgur.com/3/gallery/" + idAlbum + "/comments/"
        sessionManager.request(url).responseJSON { response in
            if((response.result.value) != nil) {
                let json = JSON(response.result.value!)
                var arrayComments = Array<ImgurComment>()
                for (_,subJson):(String, JSON) in json["data"]{
                    let comment = ImgurComment(author: subJson["author"].stringValue, comment: subJson["comment"].stringValue)
                    arrayComments.append(comment)
                }
                completion(arrayComments,nil)
            }else{
                completion(nil,ImgurError.failure)
            }
        }
    }
    
    func getImagefrom(url : String , completion:@escaping(Image?,Error?) -> ()){
        sessionManager.request(url).responseImage { response in
            if let image = response.result.value {
                completion(image,nil)
            }else{
                completion(nil,ImgurError.failure)
            }
        }
    }
    
    func uploadImage(data:String!, title:String! ,description:String! , completion:@escaping (String?,Error?)->()){
        let url = "https://api.imgur.com/3/image"
        let parameters: Parameters = [
            "image": data,
            "title": title,
            "description": description
        ]
        sessionManager.request(url, method: .post, parameters: parameters).responseJSON { response in
            if((response.result.value) != nil) {
                let json = JSON(response.result.value!)
                let link =  json["data"]["link"].stringValue
                if link.isEmpty{
                    completion(nil,ImgurError.failure)
                }else{
                    completion(link,nil)
                }                
            }else{
                completion(nil,ImgurError.failure)
            } 
        }
    }
    
    
}
