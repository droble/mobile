//
//  UploadImageViewController.swift
//  mobile-test
//
//  Created by Daniel Roble on 06-06-17.
//  Copyright © 2017 Daniel Roble. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class UploadImageViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    let imagePickerController = UIImagePickerController()
    
    @IBOutlet weak var imageSelected: UIImageView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionTextField: UITextField!
    
    @IBOutlet weak var uploadImageBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePickerController.delegate = self
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        // set all hide for more clean UI
        titleLabel.isHidden = true
        titleTextField.isHidden = true
        descriptionTextField.isHidden = true
        descriptionLabel.isHidden = true
        uploadImageBtn.isHidden = true
    }
    // UIImagePickerController only support Portrait mode, so block lanscape for these view
    override var shouldAutorotate: Bool {
        return false
    }
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.imageSelected.image = image
            titleLabel.isHidden = false
            titleTextField.isHidden = false
            descriptionTextField.isHidden = false
            descriptionLabel.isHidden = false
            uploadImageBtn.isHidden = false
        }else{
            //Error message
            let alert = UIAlertController(title: "Ops", message: "Something went wrong", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func chooseImageFromLibrary(_ sender: Any) {
        imagePickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagePickerController, animated: true, completion: nil)
    }
  
    
    @IBAction func chooseImageFromCamera(_ sender: Any) {
        imagePickerController.sourceType = UIImagePickerControllerSourceType.camera
        self.present(imagePickerController, animated: true,completion: nil)
    }
    
    @IBAction func upLoadImage(_ sender: Any) {
        let imageData:Data =  UIImagePNGRepresentation(self.imageSelected.image!)!
        let base64String = imageData.base64EncodedString()
        self.imageSelected.isHidden = true
        // adding simple indicator
        let activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 0 , y: 0, width: 100, height: 100), type: .ballScaleMultiple, color: UIColor.blue, padding: nil)
        activityIndicatorView.center = self.view.center
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        
        ImgurService.shareInstance.uploadImage(data: base64String, title: "", description: "", completion: { (link,error) in
            activityIndicatorView.stopAnimating()
            self.imageSelected.isHidden = false
            if error != nil{
                let alert = UIAlertController(title: "Ops", message: "Something went wrong", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            let alert = UIAlertController(title: "Success", message: link, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            //            self.dismiss(animated: true, completion: nil)
        })
    }
    
    @IBAction func cancelUploadImage(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
