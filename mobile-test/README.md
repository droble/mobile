Daniel Roble - test-mobile para techk
============

La app está escrita en Swift 3.1 y desarrollada en Xcode 8.3.2.

Utiliza CocoaPods, con las siguientes dependencias:

* Alamofire
* AlamofireImage
* SwiftyJSON
* NVActivityIndicatorView

Supuesto Según el Modelo de la API Imgur
------------

**Vista**: Lista de imágenes

**Tarea**: Mostrar el listado de imágenes asociadas al Tag seleccionado

Según el Modelo de Imgur, [api.imgur.com] (https://api.imgur.com/models/image), las imágenes no tienen tags, pero si las **galerias**. Por lo que se implementa listando la primera imagen del **álbum** de cada galeria según el tag selecionado.


**Vista**: Pantalla de imagen

**Tarea**: Mostar los upvotes, downvotes y views de la imagen

**Tarea**: Mostar los comentarios de la imagen seleccionada y mostrar el listado de imágenes asociadas al Tag seleccionado.

Se implementa, mostrando los upvotes, downvotes, views y comentarios del **album** asociado a esa imagen.

Supuesto de Autenticación
------------
Como no se exige el uso de alguna auntetificación especifica, se utiliza una Anónima otorgada por la API de imgur, al registrar la app. Por lo que no existe la posibilidad de iniciar sesión dentro de la app.

Las imágenes son subidas a la aplicación con la misma autenticación.

Estructura de la Aplicación
------------
La applicación está divida en carpetas que representan el modelo MVC.

**ImgurGallery** es una mezcla de los atributos que poseen los modelos de **galeria**, **album** y **images** de imgur API .

Ya que no existe un framework o dependencia específico para Swift y, el mencionado por imgur ( [imgurSession](https://github.com/geoffmacd/ImgurSession))es uno que trabaja en base a **AFNetworking**, se crea **ImgurService** la cual se encarga de la autentificación y manejo de request.

![captura xcode](http://i64.tinypic.com/ncl4xw.png)